import React from 'react'
import "bootstrap/dist/css/bootstrap.min.css"

import SightingsListItem from './sightings-list-item'

export default class SightingsList extends React.Component {

	constructor( props ) {
		super( props )

		this.state = {

			sightings: [],
			sightingsInAscendingOrder : true // Determines the order in which 
											 // the sightings are listed.

		}

		
	}

	// After receiving new props (= sightings), they will be sorted.
	// After that, the ordering of sightings (triggered by the sorting button)
	// will be done just by flipping the array.
	componentWillReceiveProps( nextProps ) {

		let sightings = this.sortSightingsByDateTime( nextProps.sightings )
		this.setState({ sightings: sightings })

	}

	// Sorts sightings by their date. Order in ascending/descending depends
	// on what has been chosen by the user.
	sortSightingsByDateTime( sightings ) {

		const self = this

		var sortedSightings = sightings.slice().sort( function( first, second ) {
			
			let firstDate = new Date( first.dateTime )
			let secondDate  = new Date( second.dateTime )

			if( self.state.sightingsInAscendingOrder ){
				return firstDate - secondDate
			} else {
				return secondDate - firstDate
			}
			
		
		})
		
		return sortedSightings

	}


	render() {

		const { sightings, sightingsInAscendingOrder } = this.state

		return (

			<div>

				<div className="page-header">
				
  					<h1>Sightings</h1>

  					<button className="btn btn-primary btn-sm"
  						onClick={() => this.setState({ 
  							sightings: sightings.reverse(),
  							sightingsInAscendingOrder: !sightingsInAscendingOrder
  						})}>

						Sort by time: 
						{ sightingsInAscendingOrder ? (
							<span>
								&nbsp; Ascending &nbsp;
								<span className="glyphicon glyphicon-sort-by-order"/>
							</span>
						) : (
							<span>
								&nbsp; Descending &nbsp;
								<span className="glyphicon glyphicon-sort-by-order-alt"/>
							</span>
						)}
						
					</button>

				</div>

  				{ sightings.length > 0 ? (
  						sightings.map( ( sighting, index ) =>
  							<SightingsListItem key={index} sighting={sighting} />
  						)
  					) : (
  						<p>Nothing has been seen. :(</p>
  					)
  				}

			</div>

		)
	}

}