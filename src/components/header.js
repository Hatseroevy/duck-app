import React from 'react'
import "bootstrap/dist/css/bootstrap.min.css"

export default class Header extends React.Component {

	render() {

		return (

			<nav className="navbar navbar-inverse">

  				<div className="container">

    				<div className="navbar-header">
      					<span className="navbar-brand">Duck-app</span>
    				</div>

  				</div>

			</nav>
			
		)
	}
}