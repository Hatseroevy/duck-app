import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'

import Header from './header'
import Content from './content'

export default class App extends React.Component {

	render() {

		return (

			<div>
				< Header />
				< Content />
			</div>
			
		)
	}

}