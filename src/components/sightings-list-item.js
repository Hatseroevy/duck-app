import React from 'react'
import "bootstrap/dist/css/bootstrap.min.css"

export default class SightingsListItem extends React.Component {

	render() {

		const { sighting } = this.props

		// Make data presentable:
		// Capitalize species name.
		let species = sighting.species.charAt(0).toUpperCase() + sighting.species.substr(1)
		// Parse time.
		let dateTime = new Date( sighting.dateTime ).toLocaleString()
		// Split description into paragraphs.
		let lines = sighting.description.split(/\r\n|\r|\n/g)

		return (

			<div className="panel panel-primary">

				<div className="panel-heading">
   					
   					<h3 className="panel-title">{ species }</h3>
   					<span>{ dateTime }</span> 

				</div>

				<div className="panel-body">

					<p>Saw { sighting.count } 
						{ sighting.count > 1 ? (
							<span> individuals.</span>
						) : (
							<span> individual.</span>
						)}
					</p>

					{ lines.map( ( line, index ) =>
  						<p key={ index }>{ line }</p>
  					)}

				</div>

			</div>

		)
	}

}