import React from 'react'
import "bootstrap/dist/css/bootstrap.min.css"
import axios from 'axios' // axios for data fetching/posting

import SightingsList from './sightings-list'
import SightingForm from './sighting-form'

export default class Content extends React.Component {

	constructor( props ) {

		super( props )

		// State holds the ducks and their sightings
		this.state = {

			species: [],
			sightings: [],
			errorOccured: false // Tells if error occured when a sighting was posted.
		}

	}


	// Fetch species and sightings when component is mounted.
	componentDidMount() {

		this.fetchSpecies()
		this.fetchSightings()

	}


	// Gets species from server and sets them into state.
	fetchSpecies() {

		const self = this

		// Fetch species from server.
		axios.get( '/species' )

			.then( function ( response ) {

				if( response.status === 200 ){

					// Got species successfully, update species list.
					self.setState({ species: response.data })
				}

			})

	}


	// Gets sightings from server and sets them into state.
	fetchSightings() {

		const self = this

		axios.get( '/sightings' )

			.then( function ( response ) {

				if( response.status === 200 ){

					self.setState({ sightings: response.data })

				}

			})

	}


	// Posts a sighting to server. Assumes the sighting json is valid.
	postSighting( sighting ) {

		const self = this

		axios.post( '/sightings', sighting )

			.then( function ( response ) {

				// Successfully sent sighting, update sightings
				self.fetchSightings()
				self.setState({ errorOccured: false })

			})

			.catch( function ( error ) {

				self.setState({ errorOccured: true })

			})


	}


	render() {

		const { species, sightings } = this.state

		return (

  			<div className="container">

  				< SightingsList sightings={ sightings } />
  				
  				< SightingForm species={ species } 
  					postSighting={ this.postSighting.bind( this ) }
  					serverErrorOccured={ this.state.errorOccured } />


  			</div>

		)

	}
}