import React from 'react'
import "bootstrap/dist/css/bootstrap.min.css"

export default class SightingForm extends React.Component {


	constructor ( props ){

		super( props )

		this.state = {

			species : '', 	// Bound to form's species input.
			date: '', 		// Bound to form's date input.
			time: '',		// Bound to form's time input.
			count: '',		// Bound to form's count input.
			description: '',// Bound to form's description input.
			speciesValidated: true,	// Tells if species from input matched 
									// any of the ones gotten from server.
			dateTimeValidated: true,// Tells if time and date form inputs
									// validated.

		}

	}


	validateInput( event ) {
		
		event.preventDefault()

		const { species, time, date } = this.state
		const speciesList = this.props.species
		let dateTime = new Date( date+'T'+time+'Z' )
		let speciesValidated = false
		let dateTimeValidated = false

		for ( let i = 0; i < speciesList.length; i++ ) {

			// Check if the given species name matches any of the ones
			// gotten from server. Checking length as well to make sure 
			// it's not a matching substring.

			if ( species.toLowerCase().match( speciesList[i].name ) 
				&& species.length === speciesList[i].name.length ) {
				
				speciesValidated = true
				break
			}

		}
		
		// Check if time values are valid
		if( !isNaN( dateTime.getTime() ) ) {

			dateTimeValidated = true

		}

		// Everything is alright and the form can be sent.
		if( speciesValidated && dateTimeValidated ){

			// The count value is checked when submit button is clicked so it
			// won't be checked here.
			// Description can be left empty and doesn't need validations.
			this.sendForm()

		}
		
		this.setState({ 
			speciesValidated: speciesValidated,
			dateTimeValidated: dateTimeValidated
		})

	}

	// Send the form's content to parent for sending. 
	// Assumes the data has been validated before call.
	sendForm() {

		const { species, date, time, count, description } = this.state

		// Combine form data into sighting object.
		var sighting = {
			species: this.state.species.toLowerCase(),
			dateTime: this.state.date+'T'+this.state.time+'Z',
			count: parseInt(this.state.count),
			description: this.state.description
		}

		// Data posting functionality provided by the parent component.
		this.props.postSighting( sighting )

		// Reset state and therefore the form inputs as well.
		this.setState({
			species : '',
			date: '',
			time: '',
			count: '',
			description: ''
		})

	}


	render() {

		return (

			<div>

				<div className="row">

					<h2 className="col-sm-12">Post your own sighting</h2>

				</div>

				<form className="form-horizontal" 
					onSubmit={ (event) => this.validateInput(event) }>

					<div className="form-group">

						<label className="col-sm-2 control-label">
							Species
						</label>

						<div className="col-sm-10">
      						<input type="text" className="form-control" 
      							id="species" placeholder="Species name"
      							value={ this.state.species } required
      							onChange={( event ) => this.setState({ species: event.target.value }) } />
    					</div>

  					</div>

  					<div className="form-group">

						<label className="col-sm-2 control-label">
							Observation date
						</label>

						<div className="col-sm-10">
      						<input type="text" className="form-control" 
      							id="date" placeholder="yyyy-mm-dd"
      							pattern="[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]" 
      							value={ this.state.date } required
      							onChange={( event ) => this.setState({ date: event.target.value }) }/>
    					</div>

  					</div>

  					<div className="form-group">

						<label className="col-sm-2 control-label">
							Observation time
						</label>

						<div className="col-sm-10">
      						<input type="text" className="form-control" 
      							id="time" placeholder="hh:mm:ss" 
      							pattern="[0-9][0-9]:[0-9][0-9]:[0-9][0-9]"
      							value={ this.state.time } required
      							onChange={( event ) => this.setState({ time: event.target.value }) }/>
    					</div>

  					</div>

  					<div className="form-group">

						<label className="col-sm-2 control-label">
							Count
						</label>

						<div className="col-sm-10">
      						<input type="number" className="form-control" id="count" 
      							min="1" placeholder="Number of individuals seen"
      							value={ this.state.count } step="1" required 
      							onChange={ ( event ) => this.setState({ count: event.target.value })}
      						/>
    					</div>

  					</div>

  					<div className="form-group">

						<label className="col-sm-2 control-label">
							Description
						</label>

						<div className="col-sm-10">
      						<textarea className="form-control" 
      							id="description" placeholder="Description..."
      							value={ this.state.description }
      							onChange={( event ) => this.setState({ description: event.target.value }) }
      							/>
    					</div>

  					</div>

  					<div className="form-group">

  						<div className="col-sm-offset-2 col-sm-8">

  							{ !this.state.speciesValidated &&
  								<div className="alert alert-danger">
  									<p>No such species exists.</p>
  									<p>Check is you misspelled the name.</p>
  								</div>
  							}

  							{ !this.state.dateTimeValidated &&
  								<div className="alert alert-danger">
  									<p>No such time combination exists.</p>
  									<p>Check is you misspelled the observation time or date.</p>
  								</div>
  							}

  							{ this.props.serverErrorOccured &&
  								<div className="alert alert-danger">
  									<p>Error occured while sending your data.</p>
  									<p>Please try again.</p>
  								</div>
  							}

  						</div>

    					<div className="col-sm-2">
      						<button type="submit" className="btn btn-primary">Submit</button>
    					</div>

  					</div>

				</form>

			</div>

		)

	}

}