# Duck app

Simple backend stub for having fun and a simple user interface for it.

## Requirements

Requires [Node.js](https://nodejs.org/) installed with npm. [Git](https://git-scm.com/) is used for cloning repository.

Tested with Node.js v6.9.2, npm v4.0.5, Google Chrome v55.0.2883.87 m (64-bit) (and Windows 7).

## Install

```
$ git clone https://Hatseroevy@bitbucket.org/Hatseroevy/duck-app.git
$ cd duck-app
$ npm install
```

## Build

```
$ npm run webpack-build
```

## Run

To start server run

```
$ npm run start
```
Except if you are using Windows, then use
```
$ npm run start-win
```

or if you want to run server in some other port than default 8081

```
$ PORT=<port> node server.js
```
or on Windows
```
$ set PORT=<port> && node server.js
```
where you should replace `<port>` with wanted port number i.e. 3000.

## Usage

Navigate to localhost:<port> in your browser.

Fill the form, click buttons as you please and follow orders if getting any. Or don't follow them, it's up to you!